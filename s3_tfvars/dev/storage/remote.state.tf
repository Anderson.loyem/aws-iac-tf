terraform {
  backend "s3" {
    bucket = "terraform-aws-assurly"
    key    = "storage.dev.tfstate"
    region = "eu-west-3"
  }
}