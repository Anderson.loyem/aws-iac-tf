terraform {
  backend "s3" {
    bucket = "terraform-aws-assurly"
    key    = "security.dev.tfstate"
    region = "eu-west-3"
  }
}