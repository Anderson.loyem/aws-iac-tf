terraform {
  backend "s3" {
    bucket = "terraform-aws-assurly"
    key    = "storage.prod.tfstate"
    region = "eu-west-3"
  }
}