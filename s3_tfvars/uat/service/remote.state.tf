terraform {
  backend "s3" {
    bucket = "terraform-aws-assurly"
    key    = "service.uat.tfstate"
    region = "eu-west-3"
  }
}