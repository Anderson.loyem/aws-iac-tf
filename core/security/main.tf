module "all_table" {
  source = "../../modules/cognito"
  tags   = var.aws_common_tag
  env    = var.env
  aws_region = var.aws_region
}
