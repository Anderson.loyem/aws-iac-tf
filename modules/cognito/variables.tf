variable "env" {
  type        = string
}

variable "aws_region" {
  type        = string
  description = "region dev"
}

variable "tags" {
  type        = map(any)
  description = "aws tag commun"
  default = {
    Name = "assurly-back"
  }
}