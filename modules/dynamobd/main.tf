
resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = var.name_table
  billing_mode   = var.billing_mode 
  read_capacity  = 20
  write_capacity = 20
  hash_key       = var.hash_key
  range_key      = var.range_key

  point_in_time_recovery {
    enabled = var.env == "prod" ? true : false
  }

  dynamic "attribute" {
    for_each = var.attribute_list
    content {
      name = attribute.value["name"]
      type = attribute.value["type"]
    }
  }

  dynamic "global_secondary_index" {
    for_each = var.index_list
    content {
      name               = global_secondary_index.value["name"]
      hash_key           = global_secondary_index.value["hash_key"]
      write_capacity     = 10
      read_capacity      = 10
      projection_type    = "INCLUDE"
      non_key_attributes = global_secondary_index.value["non_key"]
    }
  }

  tags = var.tags

  lifecycle {
    prevent_destroy = true
  }
}

