
# API Gateway

resource "aws_api_gateway_rest_api" "api_assurly" {
  name = "assurly"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
  description = "API ASSURLY"
  tags        = var.aws_common_tag
}

# ressource

resource "aws_api_gateway_resource" "insuree" {
  path_part   = "insuree"
  parent_id   = aws_api_gateway_rest_api.api_assurly.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api_assurly.id
}

# method 
/*
resource "aws_api_gateway_method" "method" {
  rest_api_id      = aws_api_gateway_rest_api.api_assurly.id
  resource_id      = aws_api_gateway_resource.insuree.id 
  http_method      = "GET"
  authorization    = "NONE"
  api_key_required = true
}
*/

module "cors" {
  source  = "squidfunk/api-gateway-enable-cors/aws"
  version = "0.3.3"

  api_id          = aws_api_gateway_rest_api.api_assurly.id
  api_resource_id = aws_api_gateway_resource.insuree.id
  allow_credentials = true
  allow_headers   = ["Authorization", "Content-Type", "X-Amz-Date", "X-Amz-Security-Token", "X-Api-Key"]
  allow_origin    = "*"
}

/*
resource "aws_api_gateway_resource" "resource2" {
  path_part   = "email"
  parent_id   = aws_api_gateway_resource.resource.rest_api_id 
  #aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}
*/

/*** deploy endpoint ***/

resource "aws_api_gateway_deployment" "assurly_deploy" {
  rest_api_id = aws_api_gateway_rest_api.api_assurly.id
  stage_name  = "v0"
}
