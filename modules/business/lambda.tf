/* post insuree */
module "lambda_post_insuree" {
  source  = "../lambda"
  name    = "insuree_post"
  role    = aws_iam_role.iam_for_lambda.arn
  handler = "lambda_function.lambda_handler"
  runtime = "python3.8"
  tags    = var.aws_common_tag
}
/* add trigger to lambda */

module "trigger_post_insuree" {
  source                      = "../api_gateway"
  aws_api_gateway_rest_api_id = aws_api_gateway_rest_api.api_assurly.id
  aws_api_gateway_resource_id = aws_api_gateway_resource.insuree.id
  lambda_invoke_arn           = module.lambda_post_insuree.invoke_arn
  lambda_name                 = module.lambda_post_insuree.function_name
  integration_http_method     = "POST"
}

/* get insuree 

module "lambda_get_insuree" {
  source  = "../lambda"
  name    = "insuree_get"
  role    = aws_iam_role.iam_for_lambda.arn
  handler = "lambda_function.lambda_handler"
  runtime = "python3.8"
  tags    = var.aws_common_tag
}

/* add trigger to lambda 

module "trigger_get_insuree" {
  source                      = "../api_gateway"
  aws_api_gateway_rest_api_id = aws_api_gateway_rest_api.api_assurly.id
  aws_api_gateway_resource_id = aws_api_gateway_resource.insuree.id
  lambda_invoke_arn           = module.lambda_post_insuree.invoke_arn
  lambda_name                 = module.lambda_post_insuree.function_name
  integration_http_method     = "GET"
}
*/
